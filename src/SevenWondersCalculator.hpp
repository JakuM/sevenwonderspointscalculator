// #include <vector>


class SevenWondersCalculator
{
	private:
		typedef struct
		{
			int pointsSum;
		} tBlueCards;

		typedef struct
		{
			int wheelCounter;
			int compassCounter;
			int plateCounter;
			int pointsSum;
		} tGreenCards;

		typedef struct{
			int warSymbolsCounter;
		} tRedCards;

		tBlueCards blueCards;
		tGreenCards greenCards; 
		tRedCards redCards;
		int warPoints;
		int eraNumber;

		SevenWondersCalculator * calcLeft;
		SevenWondersCalculator * calcRight;


	public:
		enum class greenSymbol{wheel, compass, plate};	
		void addBlueCard(int pointsCount);
		void addGreenCard(greenSymbol symbol);
		void addRedCard(int noOfWarSymbols);
		void setNeighbours(SevenWondersCalculator * calculatorLeft, SevenWondersCalculator * calculatorRight);
		void finishEra();
		int getPointsResult();	
		SevenWondersCalculator();
};