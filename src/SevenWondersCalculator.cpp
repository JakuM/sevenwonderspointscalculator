#include <SevenWondersCalculator.hpp>

SevenWondersCalculator::SevenWondersCalculator()
{
	blueCards.pointsSum = 0;
	greenCards.wheelCounter = 0;
	greenCards.compassCounter = 0;
	greenCards.plateCounter = 0;
	greenCards.pointsSum = 0;
	redCards.warSymbolsCounter = 0;
	warPoints = 0;
	eraNumber = 1;
	calcLeft = 0;
	calcRight = 0;
}

int SevenWondersCalculator::getPointsResult()
{
	return blueCards.pointsSum + greenCards.pointsSum + warPoints;
}

void SevenWondersCalculator::addBlueCard(int pointsCount)
{
	blueCards.pointsSum += pointsCount;
}

void SevenWondersCalculator::addGreenCard(greenSymbol symbol)
{
	int currPoints = 0;

	if(symbol == greenSymbol::wheel)
		greenCards.wheelCounter++;
	else if(symbol == greenSymbol::plate)
		greenCards.plateCounter++;
	else if(symbol == greenSymbol::compass)
		greenCards.compassCounter++;

	currPoints += greenCards.wheelCounter * greenCards.wheelCounter;
	currPoints += greenCards.compassCounter * greenCards.compassCounter;
	currPoints += greenCards.plateCounter * greenCards.plateCounter; 

	if((greenCards.wheelCounter <= greenCards.compassCounter) && \
		(greenCards.wheelCounter <= greenCards.plateCounter))
		
		currPoints += greenCards.wheelCounter * 7;
	else if(greenCards.compassCounter <= greenCards.plateCounter)
		currPoints += greenCards.compassCounter * 7;
	else
		currPoints += greenCards.compassCounter * 7;

	greenCards.pointsSum = currPoints;
}

void SevenWondersCalculator::addRedCard(int noOfWarSymbols)
{
	redCards.warSymbolsCounter += noOfWarSymbols;
}

void SevenWondersCalculator::setNeighbours(SevenWondersCalculator * calculatorLeft, SevenWondersCalculator  * calculatorRight)
{
	calcLeft = calculatorLeft;
	calcRight = calculatorRight;
}

void SevenWondersCalculator::finishEra()
{
	int winWarPoints;

	if((calcLeft == 0) || (calcRight == 0))
		return;
	
	if(1 == eraNumber)
		winWarPoints = 1;
	else if(2 == eraNumber)
		winWarPoints = 3;
	else if(3 == eraNumber)
		winWarPoints = 5;
	else
		return;

	eraNumber++;

	if(redCards.warSymbolsCounter > calcLeft->redCards.warSymbolsCounter)
		warPoints += winWarPoints;
	else if(redCards.warSymbolsCounter < calcLeft->redCards.warSymbolsCounter)
		warPoints--;

	if(redCards.warSymbolsCounter > calcRight->redCards.warSymbolsCounter)
		warPoints += winWarPoints;
	else if(redCards.warSymbolsCounter < calcRight->redCards.warSymbolsCounter)
		warPoints--;

}