#include "doctest.h"

#include <stdexcept>

#include <SevenWondersCalculator.hpp>

TEST_CASE("testing if true is true") {

    SevenWondersCalculator calc;

    SUBCASE("at beginning every player should have 0 points")
    {
        CHECK(0 == calc.getPointsResult());
    }

    SUBCASE("add blue card")
    {
        SUBCASE("after add 7 points blue card player should have 7 points")
        {
            calc.addBlueCard(7);
            CHECK(7 == calc.getPointsResult());
        }

        SUBCASE("after add 10 additional blue card points player should have 17 points")
        {
            calc.addBlueCard(7);
            calc.addBlueCard(10);
            CHECK(17 == calc.getPointsResult());
        }
    }

    SUBCASE("add green card")
    {
         calc.addGreenCard(SevenWondersCalculator::greenSymbol::wheel);
         CHECK(1 == calc.getPointsResult());
         calc.addGreenCard(SevenWondersCalculator::greenSymbol::wheel);
         CHECK(4 == calc.getPointsResult());
         calc.addGreenCard(SevenWondersCalculator::greenSymbol::wheel);
         CHECK(9 == calc.getPointsResult());
         calc.addGreenCard(SevenWondersCalculator::greenSymbol::plate);
         CHECK(10 == calc.getPointsResult());
         calc.addGreenCard(SevenWondersCalculator::greenSymbol::plate);
         CHECK(13 == calc.getPointsResult());
         calc.addGreenCard(SevenWondersCalculator::greenSymbol::compass);
         CHECK(21 == calc.getPointsResult());
         calc.addGreenCard(SevenWondersCalculator::greenSymbol::compass);
         CHECK(31 == calc.getPointsResult());
    }

    SUBCASE("finish era vol1")
    {
        SevenWondersCalculator calcLeft;
        SevenWondersCalculator calcRight;
        calc.addRedCard(5);
        calcLeft.addRedCard(6);
        calcRight.addRedCard(6);
        calc.setNeighbours(&calcLeft,&calcRight);
        calc.finishEra();
        CHECK(-2 == calc.getPointsResult());
        calc.finishEra();
        CHECK(-4 == calc.getPointsResult());
        calc.finishEra();
        CHECK(-6 == calc.getPointsResult());
    }

    SUBCASE("finish era vol2")
    {
        SevenWondersCalculator calcLeft;
        SevenWondersCalculator calcRight;
        calc.addRedCard(10);
        calcRight.addRedCard(6);
        calcLeft.addRedCard(6);
        calc.setNeighbours(&calcLeft,&calcRight);
        calc.finishEra();
        CHECK(2 == calc.getPointsResult());
        calc.finishEra();
        CHECK(8 == calc.getPointsResult());
        calc.finishEra();
        CHECK(18 == calc.getPointsResult());
    }

    SUBCASE("finish era vol3")
    {
        SevenWondersCalculator calcLeft;
        SevenWondersCalculator calcRight;
        calc.addRedCard(10);
        calcLeft.addRedCard(6);
        calcRight.addRedCard(6);
        calc.setNeighbours(&calcLeft, &calcRight);
        calc.finishEra();
        CHECK(2 == calc.getPointsResult());
        calc.finishEra();
        CHECK(8 == calc.getPointsResult());
        calc.finishEra();
        CHECK(18 == calc.getPointsResult());
    }
}
